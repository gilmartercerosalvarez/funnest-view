import {NgModule}              from '@angular/core';
import {RouterModule, Routes}  from '@angular/router';

import {HomeComponent}         from './modules/home/home.component';
import {ChampionshipComponent} from './modules/championship/championship.component';
import {PlaceComponent}        from './modules/place/place.component';
import {EventComponent}        from "./modules/events/event.component";
import {EventComponentNew}     from "./modules/events/event.component.new";
import {GroupComponent}        from "./modules/championship/groups/groups.component";
import {TeamComponent}         from "./modules/championship/team/team.component";

/**
 * List of routes on the application
 *
 * @type {}
 */
const appRoutes:Routes = [
    {
        path: '',
        redirectTo: 'event',
        pathMatch: 'full',
    },
    {
        path: 'Home',
        component: HomeComponent,
    },
    {
        path: 'event',
        component: EventComponent,
        children: [
            { path: '', pathMatch: 'full' },
            { path: 'new', component: EventComponentNew},
        ]
    },
    {
        path: 'place',
        component: PlaceComponent,
    },
    {
        path: 'championship',
        component: ChampionshipComponent,
    },
    {
        path: 'groups/:idChampionship',
        component: GroupComponent
    },
    {
        path: 'team/:idTeam',
        component: TeamComponent
    },
];

@NgModule({
    imports: [ RouterModule.forRoot(appRoutes) ],
    exports: [ RouterModule ]
})

export class AppRouting {}
