import {Component, OnInit}                  from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import {ChampionshipService}                from "./championship.service";

@Component({
    selector: 'championship',
    templateUrl: 'app/modules/championship/championship.component.html',
    providers: [ChampionshipService]
})

/**
 * Responsible to manage all data regarding championships
 */
export class ChampionshipComponent implements OnInit
{
    championships:any[];
    name:string;

    /**
     * Initialization/deceleration before the constructor it's where/when components' bindings are resolved
     */
    public ngOnInit()
    {
        let id = +this.route.snapshot.params['id'];
        this.getChampionshipList();
    }

    /**
     * Ensures proper initialization of fields in the class and its subclasses
     *
     * @param route
     * @param router
     * @param championshipService
     */
    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        private championshipService:ChampionshipService
    )
    {
    }

    /**
     * Redirect to groups component.
     */
    public redirectToGroups(idChampionship:number)
    {
        this.router.navigate(['/groups', idChampionship]);
    }

    /**
     * Set the attribute championships with all data recovery from backend
     */
    public getChampionshipList(): void
    {
        if(this.name == ''){
            this.name = '*';
        }
        this.championshipService.getChampionshipList(this.name).then(championships =>this.championships = championships);
    }
}
