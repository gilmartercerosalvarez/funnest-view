import {Component, OnInit}              from '@angular/core';
import {GroupsService}                  from "./groups.service";
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
    selector: 'teamGroup',
    templateUrl: 'app/modules/championship/groups/groups.component.html',
    providers: [GroupsService],
})

/**
 * Class responsible to manage all data related to TeamGroups.
 */
export class GroupComponent implements OnInit
{
    groups: any[];
    errorMessage: string;
    idChampionship: number;
    groupsOption: boolean = true;
    resultOption: boolean = false;
    fixtureOption: boolean = false;
    golOption: boolean = false;
    galleryOption: boolean = false;
    fieldOption: boolean = false;

    /**
     * Responsible to set all attributes to be used in the class.
     *
     * @param groupService
     * @param route
     * @param router
     */
    public constructor(
        private groupService:GroupsService,
        private route: ActivatedRoute,
        private router: Router
    )
    {
    }

    /**
     * Initialization/deceleration before the constructor it's where/when components' bindings are resolved
     */
    public  ngOnInit(): void
    {
        this.idChampionship = +this.route.snapshot.params['idChampionship'];
        this.getGroups(this.idChampionship);
    }

    /**
     * Get groups data to be displayed.
     *
     * @param {number} idChampionship
     */
    public getGroups(idChampionship:number)
    {
        this.groupService.getGroupsMock(idChampionship) // To work locally
        //this.placeService.getTeamGroup(id) // Work with RestApi
            .then(
                groups => this.groups = groups,
                error =>  this.errorMessage = <any>error
            );
    }

    /**
     * Set view to be displayed.
     *
     * @param option
     */
    public displayOption(option:string)
    {
        this.groupsOption = false;
        this.resultOption = false;
        this.fixtureOption = false;
        this.golOption = false;
        this.galleryOption = false;
        this.fieldOption = false;

        switch(option){
            case 'group':
                this.groupsOption = true;
                break;
            case 'results':
                this.resultOption = true;
                break;
            case 'fixture':
                this.fixtureOption = true;
                break;
            case 'goolers':
                this.golOption = true;
                break;
            case 'gallery':
                this.galleryOption = true;
                break;
            case 'fields':
                this.fieldOption = true;
                break;
            default:
                this.groupsOption = true;
                break;
        }
    }

    /**
     * Redirect the page to team descriptions
     *
     * @param idTeam
     */
    public redirectToTeam(idTeam:number)
    {
        this.router.navigate(['/team', idTeam]);
    }
}
