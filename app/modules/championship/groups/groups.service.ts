import {Injectable}              from '@angular/core';
import {Http, Response }         from '@angular/http';
import {Observable }             from 'rxjs/Observable';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';

import {Configuration}           from './../../../app.configuration';

@Injectable()

/**
 * Responsible to get information related to groups.
 */
export class GroupsService
{
    /**
     * Initialize all properties required on Group service class.
     *
     * @param http
     */
    constructor (private http:Http) {
        this.http = http;
    }

    /**
     * Return all groups registered on database from idChampionship .
     *
     * @returns {Promise<ErrorObservable|T>|any|Promise<ErrorObservable>|Promise<T>|Promise<R>|Observable<R>}
     */
    public getTeamGroups(idChampionship:number): Promise<any[]> {
        return this.http.get(Configuration.URL_SERVER + 'group?id='+idChampionship)
            .toPromise()
            .then(GroupsService.extractData)
            .catch(GroupsService.handleError);
    }

    /**
     * Return the data like Json format.
     *
     * @param response
     *
     * @returns {any|{}}
     */
    private static extractData(response: Response)
    {
        let body = response.json();

        return body.data || { };
    }

    /**
     * Handle error Occurred in the http response.
     *
     * @param error
     *
     * @returns {ErrorObservable}
     */
    private static handleError (error: Response | any)
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    /**
     * Return mock groups.
     *
     * @returns {any}
     */
    public getGroupsMock(idChampionship:number): Promise<any[]>
    {
        return Promise.resolve(GroupsService.getGroupsMockData());
    }

    /**
     * Return mock data required to works page
     *
     * @returns {any[]}
     */
    private static getGroupsMockData(): Promise<any>
    {
        var groups:any = [
            {
                id: 1,
                name: 'Honores',
                description: 'Description for group 1',
                teams: [
                    {id:1, name:'Funnest', manager:'Glimar Terceros', place:'Tolata', players:5, PJ:10, PG:45, PE:45, PP:4, GF:5, GC:74, DI:1, PT:14, description:'Description'},
                    {id:2, name:'Barcelona', manager:'Glimar Terceros', place:'San Isidro', players:5, PJ:9, PG:5, PE:1, PP:9, GF:5, GC:8, DI:1, PT:13, description:'Description'},
                    {id:3, name:'Real Madrid', manager:'Glimar Terceros', place:'Tolata', players:5, PJ:7, PG:5, PE:3, PP:12, GF:5, GC:74, DI:1, PT:12, description:'Description'},
                    {id:4, name:'Manchester', manager:'Glimar Terceros', place:'Cliza', players:5, PJ:5, PG:5, PE:4, PP:2, GF:1, GC:0, DI:1, PT:11, description:'Description'},
                    {id:5, name:'La loca bicicleta FC', manager:'Glimar Terceros', place:'Cochabamba', players:5, PJ:2, PG:14, PE:11, PP:9, GF:8, GC:4, DI:8, PT:10, description:'Description'},
                    {id:6, name:'Bolivia', manager:'Evo Morales', place:'Tolata', players:5, PJ:1, PG:12, PE:10, PP:78, GF:5, GC:7, DI:7, PT:9, description:'Description'},
                    {id:6, name:'Bolivia', manager:'Juancito Pinto', place:'Por venir', players:5, PJ:1, PG:12, PE:10, PP:78, GF:5, GC:7, DI:7, PT:9, description:'Description'},
                ],
            },
            {
                id: 2,
                name: 'Ascenso',
                description: 'Description for group 2',
                teams: [
                    {id:11, name:'Funnest', manager:'Sacarias Flores del Campo', place:'Tolata', players:5, PJ:10, PG:45, PE:45, PP:4, GF:5, GC:74, DI:1, PT:14, description:'Description'},
                    {id:21, name:'Barcelona', manager:'Glimar Terceros', place:'Tolata', players:5, PJ:9, PG:5, PE:1, PP:9, GF:5, GC:8, DI:1, PT:13, description:'Description'},
                    {id:31, name:'Real Madrid', manager:'Glimar Terceros', place:'Villa concepcion', players:5, PJ:7, PG:5, PE:3, PP:12, GF:5, GC:74, DI:1, PT:12, description:'Description'},
                    {id:41, name:'Manchester', manager:'Glimar Terceros', place:'Tolata', players:5, PJ:5, PG:5, PE:4, PP:2, GF:1, GC:0, DI:1, PT:11, description:'Description'},
                    {id:51, name:'La loca bicicleta FC', manager:'Glimar Terceros', place:'Tolata', players:5, PJ:2, PG:14, PE:11, PP:9, GF:8, GC:4, DI:8, PT:10, description:'Description'},
                    {id:61, name:'Bolivia', manager:'Glimar Terceros', place:'Valle Hermoso', players:5, PJ:1, PG:12, PE:10, PP:78, GF:5, GC:7, DI:7, PT:9, description:'Description'},
                    {id:71, name:'Bolivia', manager:'Paredes Rojas Elias', place:'Km 25', players:5, PJ:1, PG:12, PE:10, PP:78, GF:5, GC:7, DI:7, PT:9, description:'Description'},
                ]
            },
        ];
        
    return groups;
    }
}
