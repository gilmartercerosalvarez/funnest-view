/**
 * Player object
 */
export class Player
{
    id:number;
    firstName:string;
    lastName:string;
    documentId:number;
    age:number;
    address:string;
    telephone:number;
    position: string;
    description:string;
    urlPhoto:string;
}
