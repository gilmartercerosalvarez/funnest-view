import {Component}  from '@angular/core';
import {Player}     from "./../player/player.dto";

@Component({
    selector: 'player',
    templateUrl: 'app/modules/championship/player/player.component.html',
    inputs: ['player'],
})

/**
 * Player component responsible to manage all data required to display a player.
 */
export class PlayerComponent
{
    player:Player;
}
