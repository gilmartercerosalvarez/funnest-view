import { Injectable } from '@angular/core';

@Injectable()

/**
 * Responsible to get information related to championships from data base.
 */
export class ChampionshipService
{
    /**
     * Return all championships registered
     *
     * @returns {Promise<{title: string, place: string, urlImage: string, description: string}[]>}
     */
    public getChampionshipList(name:string): Promise<any[]>
    {
        return Promise.resolve(ChampionshipService.buildChampionshipList());
    }

    /**
     * Build data mock for championships
     */
    private static buildChampionshipList(): any[]
    {
        return [
            {id: 1, name: 'Liga tornado Jala', responsible: 'Gilmar Terceros', picture: 'app/modules/assets/images/championship/championships/championship1.jpg', description: 'Cochabamba 2017'},
            {id: 2, name: 'Copa Integracion', responsible: 'Gilmar Terceros', picture: 'app/modules/assets/images/championship/championships/championship2.jpg', description: 'Tolata 2017'},
            {id: 3, name: 'Liga tigritos', responsible: 'Gilmar Terceros', picture: 'app/modules/assets/images/championship/championships/championship3.jpg', description: 'Cliza 2017'},
            {id: 4, name: 'Liga femenina de futbol', responsible: 'Gilmar Terceros', picture: 'app/modules/assets/images/championship/championships/championship4.png', description: 'Cochabamba 2015'},
            {id: 5, name: 'Campeonato intercomunal W.V.', responsible: 'Gilmar Terceros', picture: 'app/modules/assets/images/championship/championships/championship2.jpg', description: 'Cochabamba 2014'},
        ];
    }
}