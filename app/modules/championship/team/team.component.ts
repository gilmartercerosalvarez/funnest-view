import {Component, OnInit}              from '@angular/core';
import {Player}                         from "./../player/player.dto";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {TeamService}                    from "./team.service";

@Component({
    selector: 'team',
    templateUrl: 'app/modules/championship/team/team.component.html',
    providers: [TeamService]
})

/**
 * Team component responsible to manage al data required to display a team
 */
export class TeamComponent implements OnInit
{
    team: any;
    player: Player;
    idTeam: number;
    displayPlayer: boolean
    errorMessage: string;

    /**
     * Start all the attributes required to work in this scope.
     *
     * @param teamService
     * @param route
     */
    public constructor(
        private teamService: TeamService,
        private route: ActivatedRoute
    )
    {
    }

    /**
     * Initialization/deceleration before the constructor it's where/when components' bindings are resolved
     */
    public ngOnInit(): void
    {
        this.idTeam = +this.route.snapshot.params['idTeam'];
        this.getTeamData(this.idTeam);
    }

    /**
     * Get team data to be displayed.
     *
     * @param idTeam
     */
    public getTeamData(idTeam:number)
    {
        console.log(this.idTeam);
        this.teamService.getTeamMock(idTeam) // To work locally
        //this.teamService.getTeam(idTeam)// Work with RestApi
            .then(
                team => this.team = team,
                error =>  this.errorMessage = <any>error
            );
    }

    /**
     * Set the player to be displayed on UI
     *
     * @param player
     */
    public setPlayer(player:Player)
    {
        this.player = player;
        this.displayPlayer = true;
    }

    /**
     * Set displayPlayer to allow display picture team.
     */
    public displayTeamPicture()
    {
        this.displayPlayer = false;
    }
}
