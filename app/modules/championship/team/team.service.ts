import {Injectable}      from '@angular/core';
import {Http, Response } from '@angular/http';
import {Observable }     from 'rxjs/Observable';
import 'rxjs/Rx';

import {Configuration}   from './../../../app.configuration';

@Injectable()

/**
 * Responsible to get information related to Team.
 */
export class TeamService
{
    /**
     * Initialize all properties required on Team service class.
     *
     * @param http
     */
    constructor (private http:Http)
    {
        this.http = http;
    }

    /**
     * Return team information for IdTeam provide.
     *
     * @returns {Promise<ErrorObservable|T>|any|Promise<ErrorObservable>|Promise<T>|Promise<R>|Observable<R>}
     */
    public getTeam(idTeam:number): Promise<any[]>
    {
        return this.http.get(Configuration.URL_SERVER + 'team?id='+idTeam)
            .toPromise()
            .then(TeamService.extractData)
            .catch(TeamService.handleError);
    }

    /**
     * Return the data like Json format.
     *
     * @param response
     *
     * @returns {any|{}}
     */
    private static extractData(response: Response)
    {
        let body = response.json();

        return body.data || { };
    }

    /**
     * Handle error Occurred in the http response.
     *
     * @param error
     *
     * @returns {ErrorObservable}
     */
    private static handleError (error: Response | any)
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    /**
     * Return team registered as mock data
     *
     * @returns {any}
     */
    public getTeamMock(idTeam:number): Promise<any[]>
    {
        return Promise.resolve(TeamService.getTeamMockData());
    }

    /**
     * Return teams mock data to display on UI
     *
     * @returns {any}
     */
    public static getTeamMockData()
    {
        var team:any =[
            {
                name: 'Barselona',
                pictureTeam: 'app/modules/assets/images/championship/teams/honduras.jpg',
                description: 'El objetivo, conseguido, es aumentar el rendimiento. Y un efecto colateral es la simplificación del API de AngularJS. Desaparecen casi todas las directivas estructurales. Especialmente las famosas ng-click ng-blur y demás directivas asociadas a eventos. Tampoco se necesitan más las directivas de ng-show y ng-hide.',
                players:[
                    {id:1, firstName:'Gilmar', lastName:'Terceros Alvarez', documentId:1283566, age:31, address:'Tolata Calle rosales', telephone:4522316, position:'Delantero', description:'Decription Gilmar', urlPhoto:''},
                    {id:1, firstName:'Elmer', lastName:'Colque pinto', documentId:79883566, age:31, address:'Uscureña', telephone:4522316, position:'Defensa', description:'Decription Gilmar', urlPhoto:'app/modules/assets/images/championship/players/player2.jpg'},
                    {id:1, firstName:'kevin', lastName:'Arispe', documentId:893566, age:31, address:'Cliza', telephone:4522316, position:'Delantero', description:'Decription Kevin', urlPhoto:''},
                    {id:1, firstName:'Ronaldo', lastName:'Canqui', documentId:782183566, age:31, address:'España', telephone:4522316, position:'Arquero', description:'Decription Ronaldo', urlPhoto:'app/modules/assets/images/championship/players/player1.jpg'},
                ]
            }
        ];

        return team;
    }
}
