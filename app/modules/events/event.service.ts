import {Injectable}              from '@angular/core';
import {Http, Response }         from '@angular/http';
import {Observable }             from 'rxjs/Observable';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';

import {Configuration}           from './../../app.configuration';

@Injectable()

/**
 * Responsible to get information related to events from data base.
 */
export class EventService
{
    /**
     * Initialize all properties required on Place service class.
     *
     * @param http
     */
    constructor (private http:Http) {
        this.http = http;
    }

    /**
     * Return all places registered on database.
     *
     * @returns {Promise<ErrorObservable|T>|any|Promise<ErrorObservable>|Promise<T>|Promise<R>|Observable<R>}
     */
    public getEvents(): Promise<Array<any>> {
        return this.http.get(Configuration.URL_SERVER + 'events')
            .toPromise()
            .then(EventService.extractData)
            .catch(EventService.handleError);
    }

    /**
     * Register places to databases.
     *
     * @param name
     *
     * @returns {any|Promise<never|T>|Observable<R>|Promise<T>|Promise<never>|Promise<R>}
     */
    public addEvent (data:Array<any>): Promise<Array<any>>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(Configuration.URL_SERVER + 'addEvent', data, options)
            .toPromise()
            .then(EventService.extractData)
            .catch(EventService.handleError);
    }

    /**
     * Return the data like Json format.
     *
     * @param response
     *
     * @returns {any|{}}
     */
    private static extractData(response: Response)
    {
        let body = response.json();

        return body.data || { };
    }

    /**
     * Handle error Occurred in the http response.
     *
     * @param error
     *
     * @returns {ErrorObservable}
     */
    private static handleError (error: Response | any)
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    /**
     * Return all places registered
     *
     * @returns {any}
     */
    public getEventsMock(): Promise<any[]>
    {
        return Promise.resolve(EventService.getEventsMockData());
    }

    /**
     * Return mock data required to works page
     *
     * @returns {any}
     */
    private static getEventsMockData(): Promise<any>
    {
        var events:any = [
            {
                id: 1,
                title: '4ta. feria de la parrillada y el choripan.',
                urlOrigin: 'www.facebook.com',
                place: 'Villa Lourdes a una cuadra de la plaza principal de Tolata.',
                image: 'app/modules/assets/images/events/tolata-18-12-2016.jpg',
                date: '18 de Diciembre 2016',
                description: 'Tolata Capital Turística del Valle Alto, Te invita a la 4ta. feria de la parrillada y el choripan que se llevara a cavo el dia domingo 18 de diciembre desde las 10:00am zona Villa Lourdes a una cuadra de la plaza principal, - se servira parillada mixta de res, cerdo, pollo y pescado.'
            },
            {
                id: 2,
                title: 'XIV Feria del pato el Cuy y la chica Tolateña',
                urlOrigin: 'www.facebook.com',
                place: 'Plaza Principal Turistica de Tolata km 33 Carretera antigua a Santa Cruz',
                image: 'app/modules/assets/images/events/tolata-04-04-2014.jpg',
                date: '6 de Julio 2014',
                description: 'Tolata Capital Turística del Valle Alto, Te invita a la XIV Feria del Pato, Cuy y el Nectar Tolateño. Domingo 6 de julio desde las 9:00am en la Plaza Principal Turística de Tolata , carretera antigua a Santa Cruz Km. 33 Se servirán variedad de platas como ser: - Lambreado de conejo - Chanka de conejo - Pato al vino - Pato asado- Pampaku'
            },
        ];

        return events;
    }
}
