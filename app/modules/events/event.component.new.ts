import {Component}     from '@angular/core';
import {EventService}  from "./event.service";
import {Configuration} from './../../app.configuration';

@Component({
    selector: 'event-new',
    templateUrl: 'app/modules/events/event.component.new.html',
    providers: [EventService]
})

/**
 * Home component responsible to manage al data required to be displayed when the page are going to load
 */
export class EventComponentNew
{
    errorMessage: string;
    sessionEnabled:boolean;
    response: any[];
    event:any;
    /**
     * Responsible to set all attributes to be used in the class.
     *
     * @param eventService
     */
    public constructor(private eventService:EventService)
    {
        this.sessionEnabled = Configuration.SESSION_ENABLED;
        this.event = {};
    }

    /**
     * Set the path image for event.
     *
     * @param event
     */
    public fileChangeEvent(event: any)
    {
        this.event.picture = event.srcElement.files;
    }

    /**
     * Register a new event in the database.
     */
    public addEvent()
    {
        console.log(this.event);
        this.eventService.addEvent(this.event)
            .then(
            response  => this.response = response,
            error =>  this.errorMessage = <any>error
        );
    }
}
