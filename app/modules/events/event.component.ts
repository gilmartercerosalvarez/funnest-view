import {Component, OnInit}  from '@angular/core';
import {EventService}       from "./event.service";
import {Configuration}      from './../../app.configuration';

@Component({
    selector: 'event',
    templateUrl: 'app/modules/events/event.component.html',
    providers: [EventService]
})

/**
 * Home component responsible to manage al data required to be displayed when the page are going to load
 */
export class EventComponent implements OnInit
{
    events:Array<any>;
    errorMessage: string;
    sessionEnabled:boolean;

    /**
     * Responsible to set all attributes to be used in the class.
     *
     * @param eventService
     */
    public constructor(private eventService:EventService)
    {
        this.sessionEnabled = Configuration.SESSION_ENABLED;
    }

    /**
     * Initialization/deceleration before the constructor it's where/when components' bindings are resolved
     */
    public  ngOnInit(): void
    {
        this.getEvents();
    }

    /**
     * Get places data to be displayed.
     */
    public getEvents()
    {
        this.eventService.getEventsMock() // To work locally
        //this.eventService.getEvents() // Work with RestApi
        .then(
            events => this.events = events,
            error =>  this.errorMessage = <any>error
        );
    }

    /**
     * Get all the events from the service and set in the attribute class that are going to use to display in UI
     */
    public getEventsMock(): void
    {
        this.eventService.getEventsMock().then(events => this.events = events);
    }
}
