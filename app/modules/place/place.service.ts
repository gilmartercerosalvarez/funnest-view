import {Injectable}                 from '@angular/core';
import {Http, Response}             from '@angular/http';
import {Headers, RequestOptions}    from '@angular/http';
import 'rxjs/Rx';

import {Configuration}              from './../../app.configuration';

@Injectable()

/**
 * Responsible to get information related to events from data base.
 */
export class PlaceService
{
    /**
     * Initialize all properties required on Place service class.
     *
     * @param http
     */
    constructor (private http:Http)
    {
        this.http = http;
    }

    /**
     * Return all places registered on database.
     *
     * @returns {Promise<ErrorObservable|T>|any|Promise<ErrorObservable>|Promise<T>|Promise<R>|Observable<R>}
     */
    public getPlaces(): Promise<Array<any>>
    {
        return this.http.get(Configuration.URL_SERVER + 'places')
            .toPromise()
            .then(PlaceService.extractData)
            .catch(PlaceService.handleError);
    }

    /**
     * Return all places registered on database.
     *
     * @returns {Promise<ErrorObservable|T>|any|Promise<ErrorObservable>|Promise<T>|Promise<R>|Observable<R>}
     */
    public getTopPlaces(): Promise<Array<any>>
    {
        return this.http.get(Configuration.URL_SERVER + 'places/top')
            .toPromise()
            .then(PlaceService.extractData)
            .catch(PlaceService.handleError);
    }

    /**
     * Register places to databases.
     *
     * @param name
     *
     * @returns {any|Promise<never|T>|Observable<R>|Promise<T>|Promise<never>|Promise<R>}
     */
    public addPlace (name: string): Promise<Array<any>>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(Configuration.URL_SERVER + 'addPlace', { name }, options)
            .toPromise()
            .then(PlaceService.extractData)
            .catch(PlaceService.handleError);
    }

    /**
     * Return the data like Json format.
     *
     * @param response
     *
     * @returns {any|{}}
     */
    private static extractData(response: Response)
    {
        let body = response.json();

        return body.data || { };
    }

    /**
     * Handle error Occurred in the http response.
     *
     * @param error
     *
     * @returns {ErrorObservable}
     */
    private static handleError (error: Response | any)
    {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }

    /**
     * Return all places registered
     *
     * @returns {any}
     */
    public getPlacesMock(): Promise<any[]>
    {
        return Promise.resolve(PlaceService.getPlacesMockData());
    }

    /**
     * Return all places registered
     *
     * @returns {any}
     */
    public getTopPlacesMock(): Promise<any[]>
    {
        return Promise.resolve(PlaceService.getTopPlacesMockData());
    }

    /**
     * Return mock data required to works page
     *
     * @returns {any}
     */
    private static getPlacesMockData(): Promise<any>
    {
        var places:any = [
            {
                id: 0,
                name: 'Tolata',
                images: [
                    {
                        title: 'Torrente lluvia sobre mi Tolata',
                        smallImage: 'app/modules/assets/images/places/tolata/1_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/1.jpg',
                        likes: 10,
                    },
                    {
                        title: 'Fiesta 14 de septiembre - Morenada',
                        smallImage: 'app/modules/assets/images/places/tolata/2_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/2.jpg',
                        likes: 15,
                    },
                    {
                        title: 'Fiesta 14 de septiembre - Morenada 1',
                        smallImage: 'app/modules/assets/images/places/tolata/3_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/3.jpg',
                        likes: 10,
                    },
                    {
                        title: 'Plaza principal',
                        smallImage: 'app/modules/assets/images/places/tolata/4_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/4.jpg',
                        likes: 105,
                    },
                    {
                        title: 'Fiesta 14 de septiembre - Salay',
                        smallImage: 'app/modules/assets/images/places/tolata/5_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/5.jpg',
                        likes: 14,
                    },
                    {
                        title: 'Cancha de futbol villla Lourdes',
                        smallImage: 'app/modules/assets/images/places/tolata/6_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/6.jpg',
                        likes: 17,
                    },
                ],
            },
            {
                id: 10,
                name: 'Cliza',
                images: [
                    {
                        title: 'Image 1',
                        smallImage: 'app/modules/assets/images/places/tolata/3_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/6.jpg',
                        likes: 13,
                    },
                    {
                        title: 'Image 2',
                        smallImage: 'app/modules/assets/images/places/tolata/3_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/6.jpg',
                        likes: 10,
                    },
                ],
            },
            {
                id: 8,
                name: 'Punata',
                images: [
                    {
                        title: 'Image 1',
                        smallImage: 'app/modules/assets/images/places/tolata/5_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/6.jpg',
                        likes: 10
                    },
                    {
                        title: 'Image 2',
                        smallImage: 'app/modules/assets/images/places/tolata/5_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/6.jpg',
                        likes: 10
                    },
                    {
                        title: 'Fiesta 14 de septiembre - Salay',
                        smallImage: 'app/modules/assets/images/places/tolata/5_s.jpg',
                        largeImage: 'app/modules/assets/images/places/tolata/5.jpg',
                        likes: 15,
                    },
                ],
            },
        ];

        return places;
    }

    /**
     * Return mock data required to works page
     *
     * @returns {any}
     */
    private static getTopPlacesMockData(): Promise<any>
    {
        var topPlaces:any = [
            {
                title: 'Torrente lluvia sobre mi Tolata',
                smallImage: 'app/modules/assets/images/places/tolata/1_s.jpg',
                largeImage: 'app/modules/assets/images/places/tolata/1.jpg',
                likes: 71,
            },
            {
                title: 'Fiesta 14 de septiembre - Morenada',
                smallImage: 'app/modules/assets/images/places/tolata/2_s.jpg',
                largeImage: 'app/modules/assets/images/places/tolata/2.jpg',
                likes: 15,
            },
            {
                title: 'Fiesta 14 de septiembre - Morenada 1',
                smallImage: 'app/modules/assets/images/places/tolata/3_s.jpg',
                largeImage: 'app/modules/assets/images/places/tolata/3.jpg',
                likes: 10,
            },
        ];

        return topPlaces;
    }
}
