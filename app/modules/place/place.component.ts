import {Component, OnInit} from '@angular/core';
import {PlaceService}      from "./place.service";

@Component({
    selector: 'event',
    templateUrl: 'app/modules/place/place.component.html',
    providers: [PlaceService],
})

/**
 * Class responsible to manage all data related to places.
 */
export class PlaceComponent implements OnInit
{
    images: Array<any>;
    places: Array<any>;
    placeName: string;
    response: Array<any>;
    currentImages:any;
    errorMessage: string;
    populars: boolean;
    topPlaces: Array<any>;

    /**
     * Responsible to set all attributes to be used in the class.
     *
     * @param placeService
     */
    public constructor(private placeService:PlaceService)
    {
        this.populars = true;
    }

    /**
     * Initialization/deceleration before the constructor it's where/when components' bindings are resolved
     */
    public  ngOnInit(): void
    {
        this.getPlaces();
        this.getTopPlaces();
    }

    /**
     * Get places data to be displayed.
     */
    public getPlaces()
    {
        this.placeService.getPlacesMock() // To work locally
        //this.placeService.getPlaces() // Work with RestApi
            .then(
                places => this.places = places,
                error =>  this.errorMessage = <any>error
            );
    }

    /**
     * Get pictures with more likes.
     */
    public getTopPlaces()
    {
        this.placeService.getTopPlacesMock() // To work locally
        //this.placeService.getTopPlaces() // Work with RestApi
        .then(
            topPlaces => this.topPlaces = topPlaces,
            error =>  this.errorMessage = <any>error
        );
    }

    /**
     * Set current place to be render on page
     *
     * @param id
     */
    public selectPlace(id:number): void
    {
        this.populars = false;
        for(let place of this.places){
            if(place.id == id){
                this.currentImages = place.images;
                this.placeName = place.name;
                break;
            }
        }
    }

    /**
     * Register a place.
     *
     * @param place
     */
    public addPlace(place:any)
    {
        this.placeService.addPlace(place.name)
            .then(
                response => this.response = response,
                error =>  this.errorMessage = <any>error
            );
    }
}
