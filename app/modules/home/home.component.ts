import {Component, OnInit } from '@angular/core';



@Component({
    selector: 'home',
    templateUrl: 'app/modules/home/home.component.html',
})

/**
 * Home component responsible to manage al data required to be displayed when the page are going to load
 */
export class HomeComponent
{
}
