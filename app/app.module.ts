import {NgModule}              from '@angular/core';
import {BrowserModule}         from '@angular/platform-browser';
import {HttpModule }           from '@angular/http';
import {FormsModule}           from '@angular/forms';

import { AppComponent }        from './app.component';
import {HomeComponent}         from './modules/home/home.component';
import {ChampionshipComponent} from './modules/championship/championship.component';
import {PlaceComponent}        from './modules/place/place.component';
import {PlayerComponent}       from "./modules/championship/player/player.component";
import {EventComponent}        from "./modules/events/event.component";
import {EventComponentNew}     from "./modules/events/event.component.new";
import {GroupComponent}        from "./modules/championship/groups/groups.component";
import {TeamComponent}         from "./modules/championship/team/team.component";

import {AppRouting}            from './app.routing';

@NgModule({
    imports: [
        BrowserModule,
        AppRouting,
        HttpModule,
        FormsModule,
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        EventComponent,
        ChampionshipComponent,
        PlaceComponent,
        PlayerComponent,
        EventComponentNew,
        GroupComponent,
        TeamComponent,
    ],
    bootstrap: [AppComponent]
})

export class AppModule {

}
