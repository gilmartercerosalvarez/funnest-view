/**
 * Save information for configuration on UI section
 */
export class Configuration
{
    // Server name
    public static URL_SERVER = 'http://localhost/work/funnest/funnest-backend/index.php?page=';
    // Admin option
    public static SESSION_ENABLED = true;
    // works only with mock data
    public static ONLY_LOCAL = true;
}
